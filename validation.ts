import * as jsonschema from 'jsonschema';
import { CALCULATION_BODY_SCHEMA } from './calculation-body-schema';
import {CalculationBodyParams} from './types';


/**
 *
 */
export const validateCalculationRequestBody = (body: CalculationBodyParams): boolean | Error => {
    const validatorResult = jsonschema.validate(body, CALCULATION_BODY_SCHEMA);
    if (validatorResult.valid) return true;

    const errors = validatorResult.errors.map(({message, argument}: {message: string; argument: string}): string => `"${argument}": ${message}`).join('; ');
    throw new Error(`Invalid query schema: ${errors}`);
};
