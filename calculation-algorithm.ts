import {RULES} from './rules';
import {
    HValueType,
    CalculationRulesMethods,
    CalculationBodyParams,
    CalculationRulesTypes,
} from './types';

/**
 *
 */
const getHValueType = (rules: CalculationRulesMethods, A: boolean, B: boolean, C: boolean): HValueType => {
    if (rules.isM(A, B, C)) return HValueType.M;
    if (rules.isP(A, B, C)) return HValueType.P;
    if (rules.isT(A, B, C)) return HValueType.T;
    throw new Error('Error: arguments A, B, C do not match of any H expressions');
};

/**
 *
 */
const getKValue = (rules: CalculationRulesMethods, hValueType: HValueType, D: number, E: number, F: number): number => {
    if (hValueType === HValueType.M) return rules.equalToM(D, E, F);
    if (hValueType === HValueType.P) return rules.equalToP(D, E, F);
    if (hValueType === HValueType.T) return rules.equalToT(D, E, F);
    throw new Error(`Error: arguments D, E, F can not give us any right calculation of K value`);
};

/**
 *
 */
export const calculationAlgorithm = ({rule = CalculationRulesTypes.BASE, A, B, C, D, E, F}: CalculationBodyParams): {H: HValueType; K: number} | Error => {
    const rules: CalculationRulesMethods = RULES[rule];
    const hValueType = getHValueType(rules, A, B, C);
    const kValue = getKValue(rules, hValueType, D, E, F);
    return {H: hValueType, K: kValue};
};
