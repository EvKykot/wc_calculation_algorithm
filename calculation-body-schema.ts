import {CalculationRulesTypes} from './types';

/**
 *
 */
export const CALCULATION_BODY_SCHEMA = {
    type: 'object',
    properties: {
        rule: {
            type: 'string',
            enum: [
                CalculationRulesTypes.BASE,
                CalculationRulesTypes.CUSTOM_1,
                CalculationRulesTypes.CUSTOM_2,
            ],
        },
        A: {type: 'boolean'},
        B: {type: 'boolean'},
        C: {type: 'boolean'},
        D: {type: 'number'},
        E: {type: 'integer'},
        F: {type: 'integer'},
    },
    required: ['A', 'B', 'C', 'D', 'E', 'F'],
};
