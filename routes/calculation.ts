import {Router} from 'express';
import {validateCalculationRequestBody} from '../validation';
import {calculationAlgorithm} from '../calculation-algorithm';


const router = Router();


/**
 *
 */
router.post('/', async (req: any, res: any, next) => {
    try {
        validateCalculationRequestBody(req.body);
        const {rule, A, B, C, D, E, F} = req.body;
        const result = calculationAlgorithm({rule, A, B, C, D, E, F});
        res.send(result);
        return next();
    } catch (e) {
        res.status(500).json({ message: 'something went wrong'});
    }
});

export default router;
