### **Implementation details**

App entry point is index.ts file

#### **Start**

_Install dependencies_: npm i

_Start the app_: npm start 

_Notes_: in npm start using not production ready solution, in production we should build app before 

_Make simple POST request to_: http://localhost:5000/calculate with JSON Body: {A, B, C, D, E, F, rule}

_Required fields_: A, B, C, D, E, F.

_Not required fields_: rule.

_Rule values_: 'base', 'custom_1', 'custom_2' (by default rule value is 'base').

#### **Request Example**:

curl --location --request POST 'http://localhost:5000/calculate' --header 'Content-Type: application/json' --data-raw '{
"rule": "custom_2",
"A": false,
"B": true,
"C": true,
"D": 7.33,
"E": 17,
"F": 14
}'

#### **Request Body Schema**

{
    type: 'object',
    properties: {
        rule: {
            type: 'string', 
            enum: ['base', 'custom_1', 'custom_2'] 
        },
        A: { type: 'boolean' },
        B: { type: 'boolean' },
        C: { type: 'boolean' },
        D: { type: 'number' },
        E: { type: 'integer' },
        F: { type: 'integer' },
    },
    required: ['A', 'B', 'C', 'D', 'E', 'F'],
}
