/**
 *
 */
export enum CalculationRulesTypes {
    BASE= 'base',
    CUSTOM_1= 'custom_1',
    CUSTOM_2= 'custom_2',
}

/**
 *
 */
export enum HValueType {
    M = 'M',
    P = 'P',
    T = 'T',
}

/**
 *
 */
export type CalculationBodyParams = {
    rule: CalculationRulesTypes;
    A: boolean;
    B: boolean;
    C: boolean;
    D: number;
    E: number;
    F: number;
}

/**
 *
 */
type GetHTypeFunc = (a: boolean, b: boolean, c: boolean) => boolean;
type GetKValueFunc1 = (d: number, e: number) => number;
type GetKValueFunc2 = (d: number, e: number, f: number) => number;

/**
 *
 */
export type CalculationRulesMethods = {
    isM: GetHTypeFunc;
    isP: GetHTypeFunc;
    isT: GetHTypeFunc;
    equalToM: GetKValueFunc1 | GetKValueFunc2;
    equalToP: GetKValueFunc1 | GetKValueFunc2;
    equalToT: GetKValueFunc2;
}

/**
 *
 */
export type CalculationRules = Record<CalculationRulesTypes, CalculationRulesMethods>;
