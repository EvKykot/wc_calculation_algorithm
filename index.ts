import express from 'express';
import calculationRoutes from './routes/calculation';


/**
 *
 */
const PORT = 5000;
const app = express();

app.use(express.json());
app.use('/calculate', calculationRoutes);

/**
 *
 */
async function start() {
    try {
        app.listen(PORT, () => (
            console.log(`App has been started on port ${PORT}.`)
        ));
    } catch (e) {
        console.log('Server Error', e.message);
        process.exit(1);
    }
}

/**
 *
 */
start();
