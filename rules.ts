import {CalculationRulesTypes} from "./types";


/**
 *
 */
const BASE = {
    isM: (a: boolean, b: boolean, c: boolean): boolean => (a && b && !c),
    isP: (a: boolean, b: boolean, c: boolean): boolean => (a && b && c),
    isT: (a: boolean, b: boolean, c: boolean): boolean => (!a && b && c),
    equalToM: (d: number, e: number): number => (d + (d * e / 10)),
    equalToP: (d: number, e: number, f: number): number => (d + (d * (e - f) / 25.5)),
    equalToT: (d: number, e: number, f: number): number => (d - (d * f / 30)),
}

/**
 *
 */
const CUSTOM_1 = {
    equalToP: (d: number, e: number): number => (2 * d + (d * e / 100)),
}

/**
 *
 */
const CUSTOM_2 = {
    isT: (a: boolean, b: boolean, c: boolean): boolean => (a && b && !c),
    isM: (a: boolean, b: boolean, c: boolean): boolean => (a && !b && c),
    equalToM: (d: number, e: number, f: number): number => (f + d + (d * e / 100)),
}

/**
 *
 */
export const RULES = {
    [CalculationRulesTypes.BASE]: BASE,
    [CalculationRulesTypes.CUSTOM_1]: {...BASE, CUSTOM_1},
    [CalculationRulesTypes.CUSTOM_2]: {...BASE, CUSTOM_2},
}
